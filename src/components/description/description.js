import utils from '/utils/utils.js';
import template from './description.template.js';

export default class DescriptionComponent extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({mode: 'open'}).innerHTML = template;

        this.$ = selector => this.shadowRoot.querySelector(selector);
        this.attr = attribute => JSON.parse(this.getAttribute(attribute));
    }

    connectedCallback() {
        this.content = this.attr('content');
        this.descriptionEl = this.$('.description');

        // console.log('Content :: ', this.content);
        this.setDescription();
    }

    setDescription() {
        this.content.description.filter(_ => !!_).forEach(desc => this.descriptionEl.appendChild(getDescription(desc)));
    }
}

customElements.define('description-wbc', DescriptionComponent);

const getDescription = desc => {
    return utils.createElement('p', {
        classList: ['content']
    }, desc);
};
