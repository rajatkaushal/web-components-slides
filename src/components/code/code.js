import template from './code.template.js';
import utils from "/utils/utils.js";

export default class CodeComponent extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({mode: 'open'}).innerHTML = template;

        this.$ = selector => this.shadowRoot.querySelector(selector);
        this.append = element => this.shadowRoot.appendChild(element);
        this.attr = attribute => JSON.parse(this.getAttribute(attribute));
    }

    connectedCallback() {
        // console.log(this.getAttribute('content'));
        this.codeEl = this.$('#code');
        this.content = this.attr('content');
        // this.btnEl = this.$('.btn');

        this.setCode();
        // this.attachEvents();
    }

    setCode() {
        this.codeEl.innerText = this.content.code;
    }
}

customElements.define('code-wbc', CodeComponent);

function getPrettyPrint() {
    return utils.createElement('script', {
        attributes: [{
            name: 'scr',
            value: 'https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js'
        }, {
            name: 'type',
            value: 'text/javascript'
        }, {
            name: 'onload',
            value: () => {
                alert("Script is ready!");
                console.log(test.defult_id);
            }
        }]
    });
}
