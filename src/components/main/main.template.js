export default `
    <link rel="stylesheet" href="/common.css">
    <link rel="stylesheet" href="/components/main/main.css">

    <div id="main-wbc" class="main-wbc">
        <div class="header-section">
            <h1 id="header" class="header"></h1>
            <slot name="sub-header"></slot>
        </div>
        <div class="slide-content"></div>

        <template id="image-template">
            <img id="image" class="image">
        </template>
    </div>

    <h2 class="author-name">Rajat Kaushal</h2>
`;
