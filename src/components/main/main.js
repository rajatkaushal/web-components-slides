import utils from '/utils/utils.js';
import content from '/content.js';
import list from '/components/list/list.js';
import code from '/components/code/code.js';
import description from '/components/description/description.js';

import template from './main.template.js';

export default class MainComponent extends HTMLElement {
    constructor() {
        super();

        this.slideCounter = 0;
        this.attachShadow({mode: 'open'}).innerHTML = template;

        this.$ = selector => this.shadowRoot.querySelector(selector);
    }

    connectedCallback() {
        this.headerEl = this.$('.header');
        this.headerSectionEl = this.$('.header-section');
        this.slideContentContainer = this.$('.slide-content');

        this.setInitialSlide();
        this.attachGlobalKeyListeners();
    }

    setHeader(header) {
        this.clearHeader();

        this.headerEl.innerHTML = header.heading.text;
        if (header.subHeading && header.subHeading.text) {
            this.headerSectionEl.append(getSubHeaderSlot(header.subHeading));
        }

        utils.setStyles(this.headerEl, header.heading.style);
        utils.setClasses(this.headerEl, header.heading.classList);

        utils.setStyles(this.headerSectionEl, header.style);
        utils.setClasses(this.headerSectionEl, header.classList);
    }

    clearHeader() {
        this.headerEl.style = {};
        this.headerEl.innerHTML = '';
        this.headerEl.className = 'header';
        this.headerSectionEl.style = {};
        this.headerSectionEl.className = 'header-section';

        const subHeaderEl = this.headerSectionEl.querySelector('h4.sub-header');

        if (subHeaderEl) {
            subHeaderEl.remove();
        }
    }

    setImage(image) {
        this.clearImage();

        const imageTemplate = this.$('#image-template');
        if (image) {
            this.$('.main-wbc').append(getImageElement(imageTemplate.content, image));
        }
    }

    clearImage() {
        const imageTemplate = this.$('.main-wbc img');

        if (imageTemplate) {
            imageTemplate.remove();
        }
    }

    setInitialSlide() {
        this.setSlideContent(this.slideContentContainer, content[0]);
    }

    attachGlobalKeyListeners() {
        const self = this;
        const slideContentContainer = this.slideContentContainer;

        document.documentElement.addEventListener('keydown', $event => {
            if (['ArrowDown', 'ArrowRight'].includes($event.code) && (self.slideCounter < content.length - 1)) {
                const currSlideConfig = content[++self.slideCounter];
                this.setSlideContent(slideContentContainer, currSlideConfig);
            } else if (['ArrowUp', 'ArrowLeft'].includes($event.code) && (self.slideCounter > 0)) {
                const currSlideConfig = content[--self.slideCounter];
                this.setSlideContent(slideContentContainer, currSlideConfig);
            }
        });
    }

    setSlideContent(container, currSlideConfig) {
        currSlideConfig.content.image = currSlideConfig.image;
        const currSlideContent = JSON.stringify(currSlideConfig.content);

        this.setHeader(currSlideConfig.header);

        switch (currSlideConfig.type) {
            case 'DESC': {
                container.innerHTML = `<description-wbc content='${currSlideContent}'></description-wbc>`;
                break;
            }
            case 'LIST': {
                container.innerHTML = `<list-wbc content='${currSlideContent}'></list-wbc>`;
                break;
            }
            case 'CODE': {
                container.innerHTML = `<code-wbc content='${currSlideContent}'></code-wbc>`;
                break;
            }
        }

        this.setImage(currSlideConfig.image);
    }
}

customElements.define('main-wbc', MainComponent);

function getSubHeaderSlot(subHeading) {
    return utils.createElement('h4', {
        style: subHeading.style,
        classList: ['sub-header', ...subHeading.classList],
        attributes: [{name: 'slot', value: 'sub-header'}]
    }, subHeading.text);
}

function getImageElement(templateContent, imageSpec) {
    const imageEl = document.importNode(templateContent, true);
    imageEl.querySelector('img').setAttribute('src', imageSpec.url);

    utils.setStyles(imageEl.querySelector('img'), imageSpec.style);
    utils.setClasses(imageEl.querySelector('img'), imageSpec.classList);

    return imageEl;
}
