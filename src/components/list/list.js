import template from './list.template.js';
import utils from "/utils/utils.js";

export default class ListComponent extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({mode: 'open'}).innerHTML = template;

        this.$ = selector => this.shadowRoot.querySelector(selector);
        this.attr = attribute => JSON.parse(this.getAttribute(attribute));
        this.append = element => this.shadowRoot.appendChild(element);
    }

    connectedCallback() {
        // console.log(this.getAttribute('content'));
        this.content = this.attr('content');
        this.headerEl = this.$('.header');
        this.listEl = this.$('.list');

        // console.log('List :: ', this.content);
        this.setDescription();
        this.setList();
    }

    setDescription() {
        if (this.content.description) {
            this.append(getDescriptionSlot(this.content.description));
        }
    }

    setList() {
        this.content.list.forEach(el => this.listEl.appendChild(getListElement(el)));
    }
}

customElements.define('list-wbc', ListComponent);

function getDescriptionSlot(description) {
    return utils.createElement('p', {
        classList: ['description'],
        attributes: [{name: 'slot', value: 'description'}]
    }, description);
}

function getListElement(el) {
    const li = utils.createElement('li', {
        classList: ['list-element'],
        style: el.style
    });

    const div = utils.createElement('div', {
        classList: ['list-container']
    });

    const header = utils.createElement('span', {
        classList: ['list-head']
    }, el.header);
    div.appendChild(header);

    if (el.description) {
        const description = utils.createElement('span', {
            classList: ['list-desc']
        }, el.description);
        div.appendChild(description);
    }

    li.appendChild(div);

    return li;
}
