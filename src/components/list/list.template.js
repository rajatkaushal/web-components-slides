export default `
  <link rel="stylesheet" href="/components/list/list.css">

  <slot name="description"></slot>
  <ol id="list" class="list"></ol>
`;
