export default [
    {
        type: 'DESC',
        id: 'wbc-desc',
        image: {
            url: '/assets/microservice-front-end.png',
            style: {
                bottom: '100px'
            },
            classList: ['ml-150', 'trans-shadow-box', 'round-edges-100-px']
        },
        header: {
            heading: {
                text: 'WEB COMPONENTS',
                style: {
                    letterSpacing: '10px',
                    textShadow: 'rgba(0, 0, 0, 0.5) 5px 5px'
                },
                classList: []
            },
            subHeading: {
                text: 'component-based <span class="font-weight-normal">software engineering</span>',
                classList: ['ml-75', 'border-bottom-dashed', 'font-size-2', 'mb-50']
            },
            classList: ['mt-150', 'ml-100', 'rounded-bordered']
        },
        content: {
            description: ['']
        }
    },
    {
        type: 'DESC',
        id: 'wbc-desc',
        image: {
            url: '/assets/web-components-evolution.png',
            classList: ['mt-50', 'image-center']
        },
        header: {
            heading: {
                text: 'web components',
                classList: ['width-full-view']
            },
            subHeading: {
                text: 'framework agnostic, web standard',
                classList: ['width-full-view', 'font-24', 'mt-25', 'mb-25']
            },
            classList: []
        },
        content: {
            description: [
                'set of web platform APIs & standards developed by W3C, allow us to create custom, reusable, encapsulated HTML widgets / components',
                'Web Components are portable, because their API is the web platform itself',
                'The components model allows for encapsulation & interoperability of individual HTML elements',
                'everything is element, DOM is the framework'
            ]
        }
    },
    {
        type: 'LIST',
        id: 'wbc-specs',
        image: {
            url: '/assets/web-components.png',
            classList: ['mt-25', 'image-center', 'image-height-450']
        },
        header: {
            heading: {
                text: 'specifications',
                classList: ['width-full-view']
            },
            subHeading: {
                text: 'major building blocks',
                classList: ['width-full-view', 'font-24', 'mt-25', 'mb-25']
            }
        },
        content: {
            description: '',
            list: [{
                header: 'Custom Elements',
                description: ''
            }, {
                header: 'ES6 Modules / HTML imports',
                description: ''
            }, {
                header: 'HTML Template',
                description: ''
            }, {
                header: 'Shadow DOM',
                description: ''
            }]
        }
    },
    {
        type: 'DESC',
        id: 'wbc-custom-elements',
        image: {
            url: '/assets/custom-elements.png',
            classList: ['mt-25', 'image-center', 'image-height-270']
        },
        header: {
            heading: {
                text: 'Custom Elements',
                classList: ['width-full-view']
            },
            subHeading: {
                text: 'APIs to define custom HTML elements.',
                classList: ['width-full-view', 'font-24', 'mt-25', 'mb-25']
            }
        },
        content: {
            description: [
                'HTML5 adds the ability to create custom HTML Element tags',
                'these are fully-valid HTML elements with custom templates, behaviors & tag names made with a set of JavaScript APIs',
                'works similar to other frameworks such as Angular directives or Ember components',
                'adopted by all major browsers, can be used with others using polyfills'
            ]
        }
    },
    {
        type: 'DESC',
        id: 'wbc-es6-modules',
        image: {
            url: '/assets/html-imports.png',
            classList: ['mt-25', 'image-center', 'image-height-270']
        },
        header: {
            heading: {
                text: 'ES6 Modules',
                classList: ['width-full-view']
            },
            subHeading: {
                text: 'declarative methods of importing web documents',
                classList: ['width-full-view', 'font-24', 'mt-25', 'mb-25']
            }
        },
        content: {
            description: [
                'formerly known as HTML Imports',
                'defines the inclusion & reuse of JS documents in a standard, modular & performant way',
                'its also easy to add external web components as a one-line import',
                'can also be used to load other modules dynamically'
            ]
        }
    },
    {
        type: 'DESC',
        id: 'wbc-html-templates',
        image: {
            url: '/assets/html-templates.png',
            classList: ['mt-25', 'image-center', 'image-height-270']
        },
        header: {
            heading: {
                text: 'HTML Templates',
                classList: ['width-full-view']
            },
            subHeading: {
                text: 'un-rendered fragment until instantiation',
                classList: ['width-full-view', 'font-24', 'mt-25', 'mb-25']
            }
        },
        content: {
            description: [
                'HTML5 provides ability to add inert HTML templates without being parsed or displayed to the DOM',
                'templates remain inert until cloned & inserted into the DOM',
                'defines a way to declare fragments of markup that go unused at page load, but can be instantiated later at runtime via JavaScript',
                'useful for dynamically display or hide elements'
            ]
        }
    },
    {
        type: 'DESC',
        id: 'wbc-shadow-dom',
        image: {
            url: '/assets/shadow-dom.png',
            classList: ['mt-25', 'image-center', 'image-height-270']
        },
        header: {
            heading: {
                text: 'Shadow DOM',
                classList: ['width-full-view']
            },
            subHeading: {
                text: 'encapsulated version of the DOM',
                classList: ['width-full-view', 'font-24', 'mt-25', 'mb-25']
            }
        },
        content: {
            description: [
                'solves the issue of encapsulation on web pages',
                'effectively isolate DOM fragments from one another, almost like an <i>iframe</i>',
                'defines how to use encapsulated style and markup in web components',
                'content inside the document’s scope is referred as light DOM, & content inside a shadow root is referred as shadow DOM'
            ]
        }
    },
    {
        type: 'LIST',
        id: 'wbc-benefits',
        header: {
            heading: {
                text: 'benefits',
                style: {
                    marginBottom: '50px'
                },
                classList: ['width-full-view']
            },
            subHeading: {
                text: ''
            }
        },
        content: {
            description: '',
            list: [{
                header: 'Composed',
                description: 'style & DOM encapsulation using Shadow DOM (nothing leaks implicitly)'
            }, {
                header: 'Reusable',
                description: 'import, use & reuse elements in apps with different frameworks'
            }, {
                header: 'Maintainability',
                description: 'composed, reusable code; reduces application size, simplifies debugging'
            }, {
                header: 'Robustness',
                description: 'framework agnostic, does not depend on framework upgrades & breaking changes'
            }, {
                header: 'Extensibility',
                description: 'can be extended with the custom elements API'
            }, {
                header: 'Interoperability',
                description: 'Native web components, interoperable at the browsers lowest level - DOM'
            }, {
                header: 'Productivity',
                description: 'reusing existing components reduces production time'
            }, {
                header: 'Accessibility',
                description: 'default browser features are accessible by extending existing elements'
            }]
        }
    },
    {
        type: 'LIST',
        id: 'wbc-challenges',
        header: {
            heading: {
                text: 'challenges',
                style: {
                    marginBottom: '50px'
                },
                classList: ['width-full-view']
            },
            subHeading: {
                text: ''
            }
        },
        content: {
            description: '',
            list: [{
                header: 'Efforts',
                description: 'Since everything needs to be done manually, initial development efforts & cost is high'
            }, {
                header: 'Shared resource dependency',
                description: 'Once developed, it gets difficult to modify the common components, due to multiple projects dependency'
            }, {
                header: 'Browser Support',
                description: 'The W3C web components specification is very new and not completely supported by all the browsers'
            }, {
                header: 'Polyfill size',
                description: 'Polyfills are workaround for features not currently supported by the browsers & they have large memory foot print'
            }, {
                header: 'SEO',
                description: 'The HTML markup inside the template is inert, which creates problems in the indexing of web pages for search engines',
                style: {
                    marginLeft: '390px'
                }
            }]
        }
    },
    {
        type: 'LIST',
        id: 'wbc-life-cycle',
        header: {
            heading: {
                text: 'life cycle hooks',
                style: {
                    marginBottom: '50px'
                },
                classList: ['width-full-view']
            },
            subHeading: {
                text: ''
            }
        },
        content: {
            description: '',
            list: [{
                header: 'constructor<sub class="header-subscript">()</sub>',
                description: 'Called when an instance of the element is created or upgraded.' +
                    'Useful for initializing state, setting up event listeners or creating Shadow DOMs'
            }, {
                header: 'connectedCallback<sub class="header-subscript">()</sub>',
                description: 'Called every time when the element is inserted into the DOM.' +
                    'Useful for running setup code, such as fetching resources or rendering'
            }, {
                header: 'disconnectedCallback<sub class="header-subscript">()</sub>',
                description: 'Called every time the element is removed from the DOM.' +
                    'Useful for running clean up code (removing event listeners, etc.)'
            }, {
                header: 'attributeChangedCallback<sub class="header-subscript">(attributeName, oldValue, newValue)</sub>',
                description: 'Called when an attribute is added, removed, updated, or replaced.' +
                    'Also called for initial values when an element is created by the parser or upgraded'
            }, {
                header: 'adoptedCallback<sub class="header-subscript">(oldDocument, newDocument)</sub>',
                description: 'Called when the element has been moved into a new document.' +
                    'Only attributes listed in the observedAttributes property receive this callback',
                style: {
                    marginLeft: '390px'
                }
            }]
        }
    },
    {
        type: 'LIST',
        id: 'wbc-pseudo-classes',
        header: {
            heading: {
                text: 'CSS pseudo classes & pseudo elements',
                style: {
                    marginBottom: '50px'
                },
                classList: ['width-full-view']
            },
            subHeading: {
                text: ''
            }
        },
        content: {
            description: '',
            list: [{
                header: ':defined <sub class="header-subscript">pseudo-class</sub>',
                description: 'selects an element that is defined, including built-in & custom elements'
            }, {
                header: ':host <sub class="header-subscript">pseudo-class</sub>',
                description: 'select a custom element (shadow root host) from inside its shadow DOM'
            }, {
                header: ':host() <sub class="header-subscript">pseudo-class</sub>',
                description: 'selects a shadow root host, only if it is matched by the selector argument'
            }, {
                header: '::part() <sub class="header-subscript">pseudo-element</sub>',
                description: 'selects any element within a shadow tree that has a matching part attribute'
            }, {
                header: '::slotted() <sub class="header-subscript">pseudo-element</sub>',
                description: 'selects any element placed inside a slot',
                style: {
                    marginLeft: '390px'
                }
            }]
        }
    },
    {
        type: 'LIST',
        id: 'wbc-frameworks-integrations',
        image: {
            url: '/assets/web-development-frameworks.png',
            classList: ['mt-50', 'image-center', 'image-height-450']
        },
        header: {
            heading: {
                text: 'integrations',
                classList: ['width-full-view']
            },
            subHeading: {
                text: 'integrable with almost all major web frameworks',
                classList: ['width-full-view', 'font-24', 'mt-25', 'mb-25']
            }
        },
        content: {
            description: '',
            list: [{
                header: 'Vue',
                description: 'easiest integrations, can be used directly, without any configurations'
            }, {
                header: 'Angular',
                description: 'needs a small configuration, just include CUSTOM_ELEMENTS_SCHEMA property'
            }, {
                header: 'React',
                description: 'complex of all 3, most obvious means of adding a custom component in React is by using DOM refs'
            }, {
                header: 'Other Frameworks',
                description: 'can be integrated with any other framework as well, but need to configure accordingly'
            }]
        }
    },
    {
        type: 'LIST',
        id: 'wbc-advanced-tooling',
        header: {
            heading: {
                text: 'advanced tooling',
                classList: ['width-full-view']
            },
            subHeading: {
                text: 'tools for authoring custom elements',
                classList: ['width-full-view', 'font-24', 'mt-25', 'mb-25']
            }
        },
        content: {
            description: '',
            list: [{
                header: 'lit-html',
                description: 'An efficient, expressive, extensible HTML templating library for JavaScript'
            }, {
                header: 'LitElement',
                description: 'Custom class providing series of APIs for creating fast, light-weight components'
            }, {
                header: 'haunted',
                description: 'React Hooks APIs implemented for web components'
            }, {
                header: 'Stencil.js',
                description: 'Toolchain for generating small, blazing fast, & 100% standards based Web Components that run in every browser'
            }, {
                header: 'Angular Elements',
                description: 'Angular components packaged as custom elements (web components)'
            }, {
                header: 'skatejs',
                description: 'Set of packages to write small, fast & scalable web components using view libraries like React, Preact & lit-HTML'
            }, {
                header: 'Polymer',
                description: 'Libraries, tools, & standards for a better web: LitElement, lit-html, web components',
                style: {
                    marginLeft: '390px'
                }
            }]
        }
    },
    {
        type: 'LIST',
        id: 'wbc-steps',
        header: {
            heading: {
                text: 'steps',
                classList: ['width-full-view']
            },
            subHeading: {
                text: 'to create custom web-components',
                classList: ['width-full-view', 'font-24', 'mt-25', 'mb-25']
            }
        },
        content: {
            description: '',
            list: [{
                header: 'Step 1',
                description: 'define HTML template with all the styling'
            }, {
                header: 'Step 2',
                description: 'define element class with default constructor invoking super()'
            }, {
                header: 'Step 3',
                description: 'attach the template defined in step 1 as shadow DOM or light DOM'
            }, {
                header: 'Step 4',
                description: 'define other hooks as required'
            }, {
                header: 'Step 5',
                description: 'add business logic, child elements, event listeners using DOM APIs'
            }, {
                header: 'Step 6',
                description: 'register the custom element using the CustomElementRegistry.define()'
            }, {
                header: 'Step 7',
                description: 'use this custom element in the app, just like any regular HTML element',
                style: {
                    marginLeft: '390px'
                }
            }]
        }
    },
    {
        type: 'CODE',
        id: 'wbc-steps-usage-example',
        header: {
            heading: {
                text: 'web-component example',
                classList: ['width-full-view']
            },
            subHeading: {
                text: 'template',
                classList: ['width-full-view', 'font-24', 'mt-25', 'mb-25']
            }
        },
        content: {
            code: `
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Simple template</title>
    <script src="main.js" defer></script>
</head>
<body>
    <h1>Simple template</h1>

    <template id="my-paragraph">
        <style>
            p {
                color: white;
                background-color: #666;
                padding: 5px;
            }
        </style>
        <p><slot name="my-text">My default text</slot></p>
    </template>

    <my-paragraph>
        <span slot="my-text">Some different text!</span>
    </my-paragraph>
</body>
</html>
            `
        }
    },
    {
        type: 'CODE',
        id: 'wbc-steps-example',
        header: {
            heading: {
                text: 'web-component example',
                classList: ['width-full-view']
            },
            subHeading: {
                text: 'class definition',
                classList: ['width-full-view', 'font-24', 'mt-25', 'mb-25']
            }
        },
        content: {
            code: `
customElements.define("my-paragraph",
    class extends HTMLElement {
        constructor() {
            super();

            const template = document.getElementById("my-paragraph");
            const templateContent = template.content;

            this.attachShadow({mode: "open"}).appendChild(
                templateContent.cloneNode(true)
            );
        }
    }
);
            `
        }
    },
    {
        type: 'DESC',
        id: 'wbc-thanks',
        header: {
            heading: {
                text: 'thank you!!',
                style: {
                    letterSpacing: '1px',
                    fontSize: '180px',
                    textShadow: 'rgba(64, 64, 64, 0.5) 10px 10px'
                },
                classList: ['mt-250', 'ml-250']
            },
            subHeading: {
                text: ''
            }
        },
        content: {
            description: ''
        }
    }
];
