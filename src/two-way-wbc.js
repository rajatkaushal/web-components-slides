const htmlTemplate = `
    <style type="text/css">
        .bg-grey {
            width: 535px;
            height: 360px;
            background: rgba(0, 0, 0, 0.75);
            border-radius: 45px;
        }
        .header {
            width: 485px;
            color: white;
            border: none;
            text-align: center;
            padding: 25px 25px 0 25px;
        }
        .common-styles {
            float: left;
            width: 450px;
            font-size: 16px;
            margin: 18px 24px;
            padding: 12px 18px;
            text-align: center;
            border-radius: 25px;
            border: 1px solid #eee;
        }
        #inputText:focus, #clearButton:focus {
            outline: none;
        }
        #outputText {
            color: white;
            font-weight: bold;
        }
        #clearButton {
            font-weight: bold;
            width: fit-content;
            margin-left: 205px;
            color: rgba(0, 0, 0, 0.75);
        }
    </style>

    <div class="bg-grey">
        <h1 class="header">demo web component</h1>
        <input type="text" id="inputText" class="common-styles" placeholder="Please Enter Something Here!">
        <br>
        <span id="outputText" class="common-styles">Hello!</span>
        <br>
        <button type="button" id="clearButton" class="common-styles">CLEAR ALL</button>
    </div>
`;

export default class TwoWayComponent extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({mode: "open"}).innerHTML = htmlTemplate;
        this.$ = selector => this.shadowRoot.querySelector(selector);
    }

    connectedCallback() {
        this.inputEl = this.$('#inputText');
        this.outputEl = this.$('#outputText');
        this.buttonEl = this.$('#clearButton');

        this.attachEventListeners();
    }

    attachEventListeners() {
        this.inputEl.addEventListener('keyup', () => {
            const value = this.inputEl.value ? this.inputEl.value : 'Value Cleared!';
            this.outputEl.innerText =  value.length < 50 ? value : `${value.substr(0, 50)}...`;
        });

        this.buttonEl.addEventListener('click', () => {
            this.inputEl.value = '';
            this.outputEl.innerText = 'Value Cleared!';
        });
    }
}

customElements.define('two-way-binding', TwoWayComponent);
