const createElement = (elementName, specs = {}, innerTemplate = '') => {
    const element = document.createElement(elementName);

    if (innerTemplate) {
        element.innerHTML = innerTemplate;
    }

    setStyles(element, specs.style);
    setClasses(element, specs.classList);

    if (specs.attributes && specs.attributes.length > 0) {
        specs.attributes.forEach(attr => element.setAttribute(attr.name, attr.value));
    }

    return element;
};

const setClasses = (element, classList = []) => {
    if (classList && classList.length > 0) {
        classList.forEach(className => element.classList.add(className));
    }
};

const setStyles = (element, style) => {
    if (style) {
        Object.keys(style).forEach(s => element.style[s] = style[s]);
    }
};

export default {
    setStyles,
    setClasses,
    createElement
};
